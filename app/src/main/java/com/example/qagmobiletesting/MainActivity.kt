package com.example.qagmobiletesting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val ok = findViewById<Button>(R.id.ok)
        ok.setOnClickListener {
            auth()
        }

        val cancel = findViewById<Button>(R.id.cancel)
        cancel.setOnClickListener {
            throw Exception("Hi this test Exception! =)")
        }
    }

    fun auth() {
        val email = findViewById<TextInputEditText>(R.id.email)
        val emailString = email.text.toString().trim()

        val pass = findViewById<TextInputEditText>(R.id.password)
        val passwordString = pass.text.toString().trim()

        if (Utils.verificationEmail(emailString) && Utils.verificationPassword(passwordString)) {
            Toast.makeText(this, "good job", Toast.LENGTH_LONG).show()
        }

        Toast.makeText(this, "Oops", Toast.LENGTH_LONG).show()
    }
}

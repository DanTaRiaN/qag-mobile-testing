## Agenta QAG Mobile Testing

### Підготовка
 - Установка гіба
 - Посилання на Репо https://gitlab.com/DanTaRiaN/qag-mobile-testing.git
 - Установка genymotion

### Виступ
 - Відкриваємо андроїд студіо
 - Скачєм проект з репозиторію
 - Розбираємо архітектуру Студіо
	 - Що це таке
	 - Які надає можливості
	 - Де і що розміщається
	 - Виведення інформації в логсат
	 - Конфігурації запуска
	 - Вибір пристрою
		 - підключення телефона(usb/wifi)(Необхідно скачати дрова з сдк)
		 - запуск з емулятор
 - Розглядаємо тестові випадки
	 - емайл
		 - username@domen
		 - username@domen.domen
		 - username.username@domen
		 - username.username@domen.domen
		 - username.username.@domen
		 - username.username.@domen.domen
		 - username.username.username@domen
		 - username.username.username@domen.domen
		 - @domen
		 - @domen.domen
		 - username.domen
		 - username.domen.domen
		 - ..domen
	 - пароль
		 - Довжина пароля більше 8 символів
		 - Довжина пароля більше 6 символів
		 - Довжина пароля більше 4 символів
 - Проводемо тестування функцій Валідації та кнопок
	 - Ручне тесстування
		 - Переглянути логи
	 - JUnit
		 - Розказати що це таке і як воно працює
	 - AndroidJUnit
		 - Розказати що це таке і як воно працює
		 - Переглянути логи
